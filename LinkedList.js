'use strict';
var Node  = require('./Node.js')
var LinkedList = function() { 
    this.listSize = 0;
    this.headNode = null;
}

 
 LinkedList.prototype.add  = function(element) {
    var node = new Node(element);
    var currentNode;

    // Verificamos si es el primer nodo en la lista
    if (!this.headNode) {
        this.headNode = node;
    } else {
        currentNode = this.headNode;

        // Este ciclo se ejecuta hasta que llegue al último elemento
        while (currentNode.next) {
            currentNode = currentNode.next;
        }

        // Obtenemos el último elemento y lo asigamos a next para crear el enlace
        currentNode.next = node;
    }

    // Incrementamos el tamaño de la lista
    this.listSize++;
}



 
LinkedList.prototype.remove = function(element){
    var index = this.indexOf(element);
    return this.removeFrom(index);
}
 
LinkedList.prototype.removeFrom = function(pos){
    // Verificamos que la posición exista
    if (pos > -1 && pos < this.listSize) {
        var currentNode = this.headNode;
        var previousNode;
        var index = 0;

        // Si pos 0, entonces eliminaremos el primer elemento.
        if (pos === 0) {
            this.headNode = currentNode.next;
        } else {
            while (index++ < pos) {
                // Mandamos el nodo actual a previous
                previousNode = currentNode;

                // Ahora el actual será el next
                currentNode = currentNode.next;
            }

            // Enlazamos el next de previous con el next del nodo actual (lo saltamos para eliminarlo)
            previousNode.next = currentNode.next;
        }

        // Restamos el elemento eliminado de la lista
        this.listSize--;

        // Retornamos el valor del elemento eliminado
        return currentNode.element;
    } 

    // Si la posición esta fuera de rangos regresamos null
    return null;
}

LinkedList.prototype.indexOf = function(element) {
    var currentNode = this.headNode;
    var index = 0;

    while (currentNode) {
        if (currentNode.element === element) {
            return index;
        }

        index++;
        currentNode = currentNode.next;
    }

    return -1;
}
 
 
LinkedList.prototype.toString = function() {
    var currentNode = this.headNode;
    var str = '\n';

    while (currentNode) {
        str += currentNode.element + '\n';
        currentNode = currentNode.next;
    }

    return str;
}


module.exports = LinkedList;